/**
 * Created by bootdo. 用户相关api
 */
import * as API from './'

export default {
 
	
  // 查询获取user列表(通过page分页)
  findDeptList: params => {
	 return API.GET('/api-cms/mis/department/getDepts', params)
  },
  
  // 增加部门
  addDept:params =>{
    return API.POST('/api-cms/mis/department/saveDepartment',params)
  },
  
  // 增加部门
  editDept:params =>{
    return API.PUT('/api-cms/mis/department/updateDepartment',params)
  },
  
  //删除部门
  delDept:params =>{
    return API.POST('/api-cms/mis/department/delDepartmentById',params)
  },

  // 查询获取user列表(通过page分页)
  findList: params => {
    return API.GET('/api-cms/mis/Communication/getUsers', params)
  },

  // 增加用户
  addUser:params =>{
    return API.POST('/api-cms/mis/Communication/saveMisUser',params)
  },
  // 修改用户
  editUser:params =>{
    return API.PUT('/api-cms/mis/Communication/updateMisUser',params)
  },
  // 删除用户
  removeUser:params =>{
    return API.POST('/api-cms/mis/Communication/delMisUserByIds',params)
  }
}
