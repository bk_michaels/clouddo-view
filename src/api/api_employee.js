/**
 * Created by fazi.
 * 员工相关api
 */
import * as API from './'

export default {
  //修改员工信息
  changeProfile: params => {
    return API.PATCH('/api/v1/users/profile', params)
  },

  //查询获取user列表(通过page分页)
  findList: params => {
    return API.GET('/api-cms/employee', params)
  },

  //增加用户
  addEmployee:params =>{
    return API.POST('/api-cms/employee',params)
  },
  //修改用户
  editEmployee:params =>{
    return API.PUT('/api-cms/employee',params)
  },
  //删除用户
  removeEmployee:params =>{
    return API.DELETE('/api-cms/employee',params)
  }
}
